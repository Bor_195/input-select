inputSelect 可输入选择框组件
===============

inputSelect基于layui中form组件:针对select选择框进行输入选择扩展

功能：
 + 本地输入选择搜索
 + 远程输入选择搜索
 
## 目录结构

初始的目录结构如下：

~~~
lib  WEB部署目录（或者子目录）
├─layui           layui源码目录
├─layui_exts      layui第三方扩展目录
│  ├─inputSelect  inputSelect 可输入选择框组件      
│  │   └─inputSelect.js
~~~

> 必须包裹在form元素中,且含有layui-form的class

> select的上级元素必须包含inputSelect和layui-form的class,

>select元素不要添加lay-verify="required"属性,会有冲突

> 本组件基于form,切勿改变原本form的使用方式


##本地输入选择搜索实例
~~~
<form class="layui-form" action="">   
    <div class="layui-form-item">
        <label class="layui-form-label">选择框</label>
        <div class="layui-input-block inputSelect layui-form">
            <select name="city">
                <option value=""></option>
                <option value="0">北京</option>
                <option value="0">北京2</option>
                <option value="1">上海</option>
                <option value="2">广州</option>
                <option value="3">深圳</option>
                <option value="4">杭州</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script>
     //注意:依賴form組件
     layui.config({
        base: './lib/layui_exts/',
    }).extend({
        inputSelect: 'inputSelect/inputSelect'
    }).use(['form', 'inputSelect'], function () {
        var form = layui.form,inputSelect = layui.inputSelect;
    });
</script>
~~~

###远程输入选择搜索
* select元素包含lay-data属性后,自动为远程搜索,传输方式json;
* method:远程请求方式,url:远程请求地址,field:搜索字段;
* 远程访问返回json:
* {
*   "status":200,
*   "msg":"success",
*   "data":[
*       {"name":"北京","value":1},
*       {"name":"北京2","value":2},
*       {"name":"北京3","value":3}
*   ]
* }
~~~
<form class="layui-form" action="">   
    <div class="layui-form-item">
        <label class="layui-form-label">选择框</label>
        <div class="layui-input-block inputSelect layui-form">
            <select name="city" lay-data="{method:'post',url:'/admin/config/test',field:'city'}">
                <option value=""></option>
                <option value="0">北京</option>
                <option value="0">北京2</option>
                <option value="1">上海</option>
                <option value="2">广州</option>
                <option value="3">深圳</option>
                <option value="4">杭州</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script>
     //注意:依賴form組件
     layui.config({
        base: './lib/layui_exts/',
    }).extend({
        inputSelect: 'inputSelect/inputSelect'
    }).use(['form', 'inputSelect'], function () {
        var form = layui.form,inputSelect = layui.inputSelect;
    });
</script>
~~~


